 Annapurna is a prominent manufacturer and supplier of [black salt](http://www.annapurnaspices.com/product/black-salt/). Black Salt also known as sanchal is dark brown almost black colored crystalline salt that changes its color to pinkish grey once it is grounded. It is enriched in sulphur that�s why they smell like eggs. It has good iron amounts and many other minerals adding to its cool nature. It is used a scondiment in Indian catches and used in chutneys, chats, raita to add aroma and taste to them. 
  
Features :
•	Because of low sodium amounts, black salt resolves indigestion, constipation and heartburns
•	Ayurvedic toothpastes contain black slat to cure gums problems and whiten teeth
•	Eating black salt in appropriate amount beautifies skin naturally
•	Adding little amount if it to food daily leads to strong and problem free hairs
•	Can be bought easily from market at cheap rates
